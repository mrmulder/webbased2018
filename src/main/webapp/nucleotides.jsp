<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 27-11-18
  Time: 11:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Nucleotides</title>
</head>
<body>
<h2>The nucleotides:</h2>
<h5>The letter A stands for ${requestScope.nucleotides.A}</h5>
<h5>The letter G stands for ${requestScope.nucleotides.G}</h5>

<table>
    <thead>
        <tr><th>Nucleotide</th><th>Full name</th></tr>
    </thead>
    <tbody>
        <c:forEach var="nucleotide" items="${requestScope.nucleotides}">
            <tr>
                <td>${nucleotide.key}</td>
                <td>${nucleotide.value}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>
</body>
</html>
